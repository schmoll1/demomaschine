﻿
namespace DemoMaschine
{
  partial class Main
  {
    /// <summary>
    /// Erforderliche Designervariable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Verwendete Ressourcen bereinigen.
    /// </summary>
    /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Vom Windows Form-Designer generierter Code

    /// <summary>
    /// Erforderliche Methode für die Designerunterstützung.
    /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
    /// </summary>
    private void InitializeComponent()
    {
      this.btnInit = new System.Windows.Forms.Button();
      this.textJobFile = new System.Windows.Forms.RichTextBox();
      this.btnRunJob = new System.Windows.Forms.Button();
      this.btnLoadJobFile = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // btnInit
      // 
      this.btnInit.Location = new System.Drawing.Point(12, 12);
      this.btnInit.Name = "btnInit";
      this.btnInit.Size = new System.Drawing.Size(716, 47);
      this.btnInit.TabIndex = 0;
      this.btnInit.Text = "Init";
      this.btnInit.UseVisualStyleBackColor = true;
      this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
      // 
      // textJobFile
      // 
      this.textJobFile.Location = new System.Drawing.Point(12, 196);
      this.textJobFile.Name = "textJobFile";
      this.textJobFile.Size = new System.Drawing.Size(282, 292);
      this.textJobFile.TabIndex = 2;
      this.textJobFile.Text = "";
      // 
      // btnRunJob
      // 
      this.btnRunJob.BackgroundImage = global::DemoMaschine.Properties.Resources.Laserbearbeiten_Laser_Machining_Icon_Bright_Back;
      this.btnRunJob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.btnRunJob.Location = new System.Drawing.Point(300, 65);
      this.btnRunJob.Name = "btnRunJob";
      this.btnRunJob.Size = new System.Drawing.Size(428, 423);
      this.btnRunJob.TabIndex = 3;
      this.btnRunJob.Text = "Run JobFile";
      this.btnRunJob.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
      this.btnRunJob.UseVisualStyleBackColor = true;
      this.btnRunJob.Click += new System.EventHandler(this.btnRunJob_Click);
      // 
      // btnLoadJobFile
      // 
      this.btnLoadJobFile.BackgroundImage = global::DemoMaschine.Properties.Resources.icons8_ordner_öffnen_100;
      this.btnLoadJobFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.btnLoadJobFile.Location = new System.Drawing.Point(12, 65);
      this.btnLoadJobFile.Name = "btnLoadJobFile";
      this.btnLoadJobFile.Size = new System.Drawing.Size(282, 125);
      this.btnLoadJobFile.TabIndex = 1;
      this.btnLoadJobFile.UseVisualStyleBackColor = true;
      this.btnLoadJobFile.Click += new System.EventHandler(this.btnLoadJobFile_Click);
      // 
      // Main
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(738, 519);
      this.Controls.Add(this.btnRunJob);
      this.Controls.Add(this.textJobFile);
      this.Controls.Add(this.btnLoadJobFile);
      this.Controls.Add(this.btnInit);
      this.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
      this.Name = "Main";
      this.Text = "DemoMaschine";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnInit;
    private System.Windows.Forms.Button btnLoadJobFile;
    private System.Windows.Forms.RichTextBox textJobFile;
    private System.Windows.Forms.Button btnRunJob;
  }
}

