﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ServoSystem
{
    /// <summary>
    /// A class to represent a 3 axis Servo-Drive
    /// </summary>
    public class Servo3Axis
    {
        public ServoAxis Axis1;
        public ServoAxis Axis2;
        public ServoAxis Axis3;

        /// <summary>
        /// Is True when all axes are in ready state.
        /// </summary>
        public bool IsReady { 
            get 
            {
                return Axis1.IsReady && Axis2.IsReady && Axis3.IsReady;
            }
        }

        /// <summary>
        /// Initializes all axes
        /// </summary>
        public void Init()
        {
            Axis1.Init();
            Axis2.Init();
            Axis3.Init();
        }

        public Servo3Axis()
        {
            Axis1 = new ServoAxis();
            Axis2 = new ServoAxis();
            Axis3 = new ServoAxis();
        }
    }

    /// <summary>
    /// A class to represent one axis of servosystem
    /// </summary>
    public class ServoAxis
    {

        public bool IsReady;

        private double ActualPosition;
        private double TargetPosition;
        private double DeltaPosition;
        private double Speed;
        private double Interval_ms;
        private Timer timer;

        public ServoAxis()
        {
            Interval_ms = 100;
            timer = new Timer();
            timer.Interval = Interval_ms;
            timer.AutoReset = true;
            timer.Elapsed += DoTimer;
            timer.Enabled = false;
            timer.Start();
            Speed = 100; //mm/s
        }

        /// <summary>
        /// Initializes axis
        /// </summary>
        public void Init()
        {
            IsReady = true;
        }

        /// <summary>
        /// Set Speed of axis in mm/s
        /// </summary>
        /// <param name="speed">Speed in mm/s</param>
        public void SetSpeed(double speed)
        {
            if (speed <= 0)
                throw new Exception("Speed must be greater than 0!");
            Speed = speed;
        }

        /// <summary>
        /// Move axis absolute to TargetPosition
        /// </summary>
        /// <param name="TargetPosition">Desired absolute position in mm</param>
        public void MoveToAbsolute(double TargetPosition)
        {
            this.TargetPosition = TargetPosition;
            this.DeltaPosition = this.TargetPosition - ActualPosition;
            timer.Enabled = true;
        }

        /// <summary>
        /// Returns actual position of axis in mm
        /// </summary>
        /// <returns>Actual position in mm</returns>
        public double GetActualPosition()
        {
            return ActualPosition;
        }

        private void DoTimer(Object source, System.Timers.ElapsedEventArgs e)
        {
            double PosChange = Speed * Interval_ms / 1000;
            
            if(Math.Abs(DeltaPosition) < PosChange)
            {
                // Move
                ActualPosition = TargetPosition;
                timer.Enabled = false;
            }
            else
            {
                if(DeltaPosition > 0) //Target > Act --> increase act
                {
                    ActualPosition += PosChange;
                }
                else
                {
                    ActualPosition -= PosChange;
                }
            }
            this.DeltaPosition = this.TargetPosition - ActualPosition;
        }
    }
}
