﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WagoIO
{
    public class WagoIO
    {
        public bool IsIdle;
        public void Initialize()
        {
            IsIdle = true;
        }

        bool IO0 = false;
        bool IO1 = false;

        /// <summary>
        /// Sets digital output to desired value.
        /// </summary>
        /// <param name="IONumber">Number of IO</param>
        /// <param name="Value">Set true or false</param>
        public void SetIO(int IONumber, bool Value)
        {
            if (IONumber == 0)
                IO0 = Value;
            else if (IONumber == 1)
                IO1 = Value;
            else
                throw new Exception($"IO {IONumber} does not exists");

        }

        /// <summary>
        /// Returns actual state of digital output.
        /// </summary>
        /// <param name="IONumber">Number of IO</param>
        /// <returns>State of desired IO</returns>
        public bool GetIOValue(int IONumber)
        {
            if (IONumber == 0)
                return IO0;
            else if (IONumber == 1)
                return IO1;
            else
            {
                throw new Exception($"IO {IONumber} does not exists");
            }
        }
    }
}
