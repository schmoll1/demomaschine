﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LaserScanner
{
    public class LaserScanner
    {
      public bool IsReady;

      Timer timer;
      public LaserScanner()
      {
        timer = new Timer();
        timer.Interval = 5000;
        timer.AutoReset = true;
        timer.Elapsed += DoTimer;
        timer.Enabled = false;
        timer.Start();
        IsReady = false;
      }

      /// <summary>
      /// Fired when scanner is finished with scan area.
      /// </summary>
      public event EventHandler ScanningFinished;

      /// <summary>
      /// Makes scanner ready to process.
      /// </summary>
      public void Init()
      {
        IsReady = true;
      }

      /// <summary>
      /// Starts scanning actual scan area.
      /// </summary>
      public void StartScanning()
      {
        timer.Enabled = true;
      }

      private void DoTimer(Object source, System.Timers.ElapsedEventArgs e)
      {
        timer.Enabled = false;
        ScanningFinished?.Invoke(this, new EventArgs());
      }
    }
}
