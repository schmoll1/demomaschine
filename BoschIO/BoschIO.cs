﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoschIO
{
    public class BoschIO
    {

        public bool IsReady;
        public void Init()
        {
            IsReady = true;
        }

        bool DigitalIO0 = false;
        bool DigitalIO1 = false;

        public void SetDigitalIO(int IONumber, bool Value)
        {
            if (IONumber == 0)
                DigitalIO0 = Value;
            else if (IONumber == 1)
                DigitalIO1 = Value;
            else
                throw new Exception($"IO {IONumber} does not exists");

        }

        public bool GetDigitalIOValue(int IONumber)
        {
            if (IONumber == 0)
                return DigitalIO0;
            if (IONumber == 1)
                return DigitalIO1;
            return false;
        }
    }
}
